# -*- coding: utf-8 -*-
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import plonetheme.owr


class PlonethemeOwrLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        self.loadZCML(package=plonetheme.owr)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'plonetheme.owr:default')


PLONETHEME_OWR_FIXTURE = PlonethemeOwrLayer()


PLONETHEME_OWR_INTEGRATION_TESTING = IntegrationTesting(
    bases=(PLONETHEME_OWR_FIXTURE,),
    name='PlonethemeOwrLayer:IntegrationTesting'
)


PLONETHEME_OWR_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(PLONETHEME_OWR_FIXTURE,),
    name='PlonethemeOwrLayer:FunctionalTesting'
)


PLONETHEME_OWR_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        PLONETHEME_OWR_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE
    ),
    name='PlonethemeOwrLayer:AcceptanceTesting'
)
