# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer


class IPlonethemeOwrLayer(IDefaultBrowserLayer):
    """Marker interface that defines a browser layer."""


class IThemeSpecific(Interface):
    """Marker interface that defines a Zope 3 browser layer.
    """
