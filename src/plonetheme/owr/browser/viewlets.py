from plone.app.layout.viewlets.common import ViewletBase


class OwrHeaderViewlet(ViewletBase):

    def Logo(self):
        # TODO: should this be changed to settings.site_title?
        pass

    def update(self):
        super(OwrHeaderViewlet, self).update()

        portal = self.portal_state.portal()
        #bprops = portal.restrictedTraverse('base_properties', None)
        # if bprops is not None:
        #     logoName = bprops.logoName
        # else:
        #     logoName = 'logo.jpg'

        #logoTitle = self.portal_state.portal_title()
        #self.logo_tag = portal.restrictedTraverse(logoName).tag(title=logoTitle, alt=logoTitle)
        self.navigation_root_title = self.portal_state.navigation_root_title()
