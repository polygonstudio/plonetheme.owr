# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plonetheme.owr.testing import PLONETHEME_OWR_INTEGRATION_TESTING  # noqa
from plone import api

import unittest2 as unittest


class TestSetup(unittest.TestCase):
    """Test that plonetheme.owr is properly installed."""

    layer = PLONETHEME_OWR_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if plonetheme.owr is installed with portal_quickinstaller."""
        self.assertTrue(self.installer.isProductInstalled('plonetheme.owr'))

    def test_uninstall(self):
        """Test if plonetheme.owr is cleanly uninstalled."""
        self.installer.uninstallProducts(['plonetheme.owr'])
        self.assertFalse(self.installer.isProductInstalled('plonetheme.owr'))

    def test_browserlayer(self):
        """Test that IPlonethemeOwrLayer is registered."""
        from plonetheme.owr.interfaces import IPlonethemeOwrLayer
        from plone.browserlayer import utils
        self.assertIn(IPlonethemeOwrLayer, utils.registered_layers())
